<%@page language="java" contentType="text/html charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Registration page</title>
        
        <script type="text/javascript">
            
            function validateFormData() {
                
                if(document.getElementById("name").value === "") {
                    
                    alert("Please, provide a name.");
                    return false;
                }
                else if(document.getElementById("email").value === "") {
                    
                    alert("Please, provide an email address");
                    return false;
                }
                else if(document.getElementById("password").value !== document.getElementById("confirmPassword").value) {
                    
                    alert("Password and confirm password fields do not match");
                    return false;
                }
                else if(validatePassword() === false) {
                    
                    alert("Password must be at least 8 characters long.\n\
                           Password must have at least one uppercase or lowercase character, \n\
                           at least one number and at least one special character.");
                    return false;
                }
            }
            
            function validatePassword() {
                
                //retrieve password
                var password = document.getElementById("password").value;
                
                //(i) password must have at least 8 characters
                //(ii) password must have at least one uppercase or lowercase letter
                //(iii) password must have at least one number
                //(iv) password must have at least one special character
                if(password.length < 8 ||
                   password.match(new RegExp("[a-zA-Z]")) === false ||
                   password.match(new RegExp("[0-9]") === false) ||
                   password.match(new RegExp("[^\w\s]") === false)) {
                    
                    return false;
                }
                
                return true;
            }
        </script>
    </head>
    <body>
        <h1>Web Application</h1>

        <div style="width:50%">
            <h3>Register Form</h3>
            <form:form method="POST" action="signUp.htm" modelAttribute="user">
                 <table border = 1>
                    <tr>
                        <td><form:label path="name">Name</form:label></td>
                        <td><form:input path="name" id="name"/></td>
                    </tr>
                    <tr>
                        <td><form:label path="email">Email</form:label></td>
                        <td><form:input path="email" id="email"/></td>
                    </tr>
                    <tr>
                        <td><form:label path="telephoneNumber">Telephone number</form:label></td>
                        <td><form:input path="telephoneNumber"/></td>
                    </tr>
                    <tr>
                        <td><form:label path="companyName">Company name</form:label></td>
                        <td><form:input path="companyName"/></td>
                    </tr>
                    <tr>
                        <td><form:label path="password">Password</form:label></td>
                        <td><form:input path="password" type="password" id="password"/></td>
                    </tr>
                    <tr>
                        <td>Confirm Password</td><td><input type="password" id="confirmPassword"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit" onclick="return validateFormData()"/></td>
                    </tr>
                </table>
            </form:form>
        </div>
      
    </body>
</html>