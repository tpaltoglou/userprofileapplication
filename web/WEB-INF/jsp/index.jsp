<%@page language="java" contentType="text/html charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Index page</title>
        
        <script type="text/javascript">
            
            function validateFormData() {
                
                //email and password must not be null
                if(document.getElementById("email").value === "") {
                    
                    alert("Please, provide an email address");
                    return false;
                }
                else if(document.getElementById("password").value === "") {
                    
                    alert("Please, provide a password");
                    return false;
                }
                
                return true;
            }
            
            
        </script>
    </head>
    <body>
        <h1>Web Application</h1>
        
        <div style="width: 30%">
            <h3>Login Form</h3>
            <form:form method="POST" action="signIn.htm" modelAttribute="user">
                <table>
                    <tr>
                        <td><form:label path="email">Email</form:label></td>
                        <td><form:input path="email" id="email"/></td>
                    </tr>
                    <tr>
                        <td><form:label path="password">Password</form:label></td>
                        <td><form:input path="password" type="password" id="password"/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit" onclick="return validateFormData()"/></td>
                    </tr>
                </table>  
            </form:form>
        </div>
         
        <div>
            Not registered? Please, <a href="register.htm">Sign Up</a>
        </div>

            

        
    </body>
</html>