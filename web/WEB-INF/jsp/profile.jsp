<%-- 
    Document   : profile
    Created on : 9 Ιαν 2019, 6:25:13 μμ
    Author     : asus
--%>

<%@page language="java" contentType="text/html charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <body>
        <div align ="right">
            You are connected. <a href="index.htm">Sign Out</a>
        </div>
        <div align="left">
            <h2>Web Application</h2>
            <h2>Name: ${resultData.user.name}</h2>
            <h2>Email: ${resultData.user.email}</h2>
            <h2>Telephone Number: ${resultData.user.telephoneNumber}</h2>
            <h2>Company Name: ${resultData.user.companyName}</h2>
            <h2>Password: ${resultData.user.password}</h2>
        </div>
    </body>
</html>
