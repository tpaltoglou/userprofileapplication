/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import helper.Result;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import src.helper.DBConnection;
import src.helper.Initializer;
import src.model.User;
import src.service.UserService;

/**
 *
 * @author asus
 */
public class UserServiceTest {
    
    public UserServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        //before any test runs, erase old data and insert new data
        Initializer.prepareData();
        
        //initialize the number of the stored user (each new user's id
        //is resolved from the number of the users in the table)
        UserService.retrieveNumOfUsers();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of register method, of class UserService.
     */
    @Test
    public void testRegister() {
        System.out.println("insertUserToDB");
        User u = new User("theopaloglou", "theopaltoglou23@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        User u2 = new User("theopaloglou", "theopaltoglou24@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        UserService instance = new UserService();
        
        //case 1: a user with a non-existing email tries to register
        Result result = instance.register(u);
        assertEquals(result.getResultCode(), true);
        
        result = instance.register(u2);
        assertEquals(result.getResultCode(), true);
        
        //case 2: a user with an existing email tries to register
        result = instance.register(u);
        assertEquals(result.getResultCode(), false);
    }

    /**
     * Test of logIn method, of class UserService.
     */
    @Test
    public void testLogIn() {
        System.out.println("logIn");
        
        //case 1: attempting to login with wrong credentials
        String email = "theopaltoglou3@hotmail.gr";
        String password = "thepaltoglou";
        Result result = UserService.logIn(email, password);
        assertEquals(result.getResultCode(), false);
        
        //case 2: attempting to login with correct credentials
        email = "theopaltoglou2@hotmail.gr";
        password = "theopaltoglou";
        result = UserService.logIn(email, password);
        assertEquals(result.getResultCode(), true);
    }
    
}
