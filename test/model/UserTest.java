/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import src.model.User; 

/**
 *
 * @author asus
 */
public class UserTest {
    
    
    public UserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getUserID method, of class User.
     */
//    @Test
//    public void testGetUserID() {
//        System.out.println("getUserID");
//        User instance = new User();
//        int expResult = 0;
//        int result = instance.getUserID();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of setUserID method, of class User.
     */
//    @Test
//    public void testSetUserID() {
//        System.out.println("setUserID");
//        int userID = 0;
//        User instance = new User();
//        instance.setUserID(userID);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of getPassword method, of class User.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        assertEquals(instance.getPassword(), "theopaltoglou");
    }

    /**
     * Test of setPassword method, of class User.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        instance.setPassword("thepaltoglou");
        assertEquals(instance.getPassword(), "thepaltoglou");
    }

    /**
     * Test of getName method, of class User.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        String result = instance.getName();
        assertEquals(instance.getName(), "theopaloglou");
    }

    /**
     * Test of setName method, of class User.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        instance.setName("teopaloglou");
        assertEquals(instance.getName(), "teopaloglou");
    }

    /**
     * Test of getEmail method, of class User.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        assertEquals(instance.getEmail(), "theopaltoglou2@hotmail.gr");
    }

    /**
     * Test of setEmail method, of class User.
     */
    @Test
    public void testSetEmail() {
        System.out.println("setEmail");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        instance.setEmail("theopaltoglou21@hotmail.gr");
        assertEquals(instance.getEmail(), "theopaltoglou21@hotmail.gr");
    }

    /**
     * Test of getTelephoneNumber method, of class User.
     */
    @Test
    public void testGetTelephoneNumber() {
        System.out.println("getTelephoneNumber");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        assertEquals(instance.getTelephoneNumber(), "6977255596");
    }

    /**
     * Test of setTelephoneNumber method, of class User.
     */
    @Test
    public void testSetTelephoneNumber() {
        System.out.println("setTelephoneNumber");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        instance.setTelephoneNumber("6977255597");
        assertEquals(instance.getTelephoneNumber(), "6977255597");
    }

    /**
     * Test of getCompanyName method, of class User.
     */
    @Test
    public void testGetCompanyName() {
        System.out.println("getCompanyName");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        assertEquals(instance.getCompanyName(), "datawise");
    }

    /**
     * Test of setCompanyName method, of class User.
     */
    @Test
    public void testSetCompanyName() {
        System.out.println("setCompanyName");
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        instance.setCompanyName("datawise2");
        assertEquals(instance.getCompanyName(), "datawise2");
    }

    /**
     * Test of equals method, of class User.
     */
    @Test
    public void testEquals() {
        System.out.println("setAddress");
        
        //1st case: equal instances
        User instance = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        User instance2 = new User("theopaloglou", "theopaltoglou2@hotmail.gr", "6977255596", "datawise", "theopaltoglou");
        assertEquals(instance, instance2);
        
        //2nd case: check equality with null
        assertNotEquals(instance, null);
        
        //3rd case: check equality with instance different to user type
        assertNotEquals(instance, 5);
        
        //4th case: check equality between instance with different email
        instance2.setEmail("theopaltoglou@hotmail.gr");
        assertNotEquals(instance, instance2);
    }
    
}
