/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.model;

/**
 *
 * @author asus
 */

public class User {
    
    private int userID;
    private String name;
    private String email;
    private String telephoneNumber;
    private String companyName;
    private String password;
    
    public User() {
        
    }
    
    public User(String name, String email, String telephoneNumber, String companyName, String password) {
        
        this.setName(name);
        this.setEmail(email);
        this.setTelephoneNumber(telephoneNumber);
        this.setCompanyName(companyName);
        this.setPassword(password);
    }

    /**
     * @return the userID
     */
    public int getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telephoneNumber
     */
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    /**
     * @param telephoneNumber the telephoneNumber to set
     */
    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    /**
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * @param companyName the companyName to set
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
    
    @Override
    public boolean equals(Object user) {
        
        if(user == null || user instanceof User == false) {
            
            return false;
        }
        
        //compare two users by their email (emai is unique)
        User instance = (User) user;
        if(this.email.equals(instance.email) == false) {
            
            return false;
        }
        
        return true;
    }
}
