/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import src.model.User;

/** Helper utility class for creating and closing connections with the database.
 *
 * @author asus
 */
public class DBConnection {
    
    private static final String db = "jdbc:derby://localhost:1527/userDB";
    private static String user = "root";
    private static String password = "root";
    
    private static Connection conn = null;
    
    //create a connection to the db, in order to perform queries
    public static Connection createConnection() {
        
        if(conn != null) {
            
            return conn;
        }
        
        try{
           DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());

           System.out.println("Connecting to database...");
           conn = (Connection) DriverManager.getConnection(db,user,password);
         }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        return conn;
    }
    
    public static void closeConnection() {
            try{
               if(conn!=null)
                  conn.close();
            }
            catch(SQLException se){
               se.printStackTrace();
            }
    }
}
