/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.helper;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author asus
 */
public class Initializer {
    
    /**
     * Erases data from user table in the database. Used before the junit tests run.
     */
    private static void eraseData() {
        
        PreparedStatement stmt = null;
        try{

           Connection conn = DBConnection.createConnection();
           
           String sql = "DELETE FROM users";
           stmt = conn.prepareStatement(sql);
           stmt.executeUpdate();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Prepares data in db before the junit tests run.
     */
    public static void prepareData() {
        
        eraseData();
        
        PreparedStatement stmt = null;
        PreparedStatement stmt2 = null;
        PreparedStatement stmt3 = null;
        try{

           Connection conn = DBConnection.createConnection();
           
           String sql = "INSERT INTO users(userid, name, email, telephonenumber, companyname, password)"
                   + "VALUES(?,?,?,?,?,?)";
           stmt = conn.prepareStatement(sql);
           stmt.setInt(1, 1);
           stmt.setString(2, "theopaltoglou");
           stmt.setString(3, "theopaltoglou2@hotmail.gr");
           stmt.setString(4, "6977255596");
           stmt.setString(5, "datawise");
           stmt.setString(6, "theopaltoglou");
           stmt.executeUpdate();
           
//           sql = "INSERT INTO users(userid, name, email, telephonenumber, companyname, password)"
//                   + "VALUES(?,?,?,?,?,?)";
           stmt2 = conn.prepareStatement(sql);
           stmt2.setInt(1, 2);
           stmt2.setString(2, "theopaltoglou");
           stmt2.setString(3, "theopaltoglou21@hotmail.gr");
           stmt2.setString(4, "6977255596");
           stmt2.setString(5, "datawise");
           stmt2.setString(6, "theopaltoglou");
           stmt2.executeUpdate();
           
//           sql = "INSERT INTO users(userid, name, email, telephonenumber, companyname, password)"
//                   + "VALUES(?,?,?,?,?,?)";
           stmt3 = conn.prepareStatement(sql);
           stmt3.setInt(1, 3);
           stmt3.setString(2, "theopaltoglou");
           stmt3.setString(3, "theopaltoglou22@hotmail.gr");
           stmt3.setString(4, "6977255596");
           stmt3.setString(5, "datawise");
           stmt3.setString(6, "theopaltoglou");
           stmt3.executeUpdate();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
