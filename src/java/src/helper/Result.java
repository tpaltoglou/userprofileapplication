/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import src.model.User;

/**
 * Helper class for the output result.
 * @author asus
 */
public class Result {
    
    //resultMessage is used for the Java Web Services (web service logic)
    private String resultMessage;
    
    //resultCode is used for the domain model and the Java services (data + business logic)
    private boolean resultCode;
    
    //user with the given credentials
    private User user = null;
    
    public Result(String resultMessage, boolean resultCode) {
        
        this.resultMessage = resultMessage;
        this.resultCode = resultCode;
    }

    /**
     * @return the resultMessage
     */
    public String getResultMessage() {
        return resultMessage;
    }
    
    public void setResultMessage(String resultMessage) {
        
        this.resultMessage = resultMessage;
    }

    /**
     * @return the resultCode
     */
    public boolean getResultCode() {
        return resultCode;
    }
    
    public void setUser(User user) {
        
        this.user = user;
    }
    
    public User getUser() {
        
        return this.user;
    }
}
