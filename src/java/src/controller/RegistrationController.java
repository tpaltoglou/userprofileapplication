/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controller;

import helper.Result;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import src.model.User;
import src.service.UserService;

/**
 *
 * @author asus
 */

@Controller
public class RegistrationController {
    
    /**
     * Web service needed in order to add the user type in the registration form.
     * @param model
     * @return 
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView displayUser(Model model) {
        
        return new ModelAndView("register", "user", new User());
    }
    
    @RequestMapping(value = "/signUp", method = RequestMethod.POST)
    public ModelAndView signUp(@ModelAttribute("user") User user, BindingResult result, ModelMap model) {
        
        Result signUpResult = UserService.register(user);
        boolean resultCode = signUpResult.getResultCode();
        String redirectPage;
        if(resultCode == true) {
            
            //successful sign up - return corresponding message
            redirectPage = "signUp";
        }
        else {
            
            redirectPage = "tryAgain";
        }
        
        //ModelAndView: Spring class that resolves the view (jsp file that will display the result)
        //and the data model (the result from the service processing, which is returned back to the
        //browser through the view)
        
        //the view is resolved in /WEB-INF/jsp/index.jsp (see dispatcher-servlet.xml for details)
        //1st argument: view that needs to be resolved
        //2nd argument: data message (the name which will be used in order to access the data model in the jsp file)
        //3rd argument: data model (usually a Java bean)
        model.addAttribute("user", user);
        return new ModelAndView(redirectPage, "message", signUpResult.getResultMessage() + " Redirecting to login page...");
    }
}
