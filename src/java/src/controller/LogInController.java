/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.controller;

import helper.Result;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import src.model.User;
import src.service.UserService;

/**
 *
 * @author asus
 */

@Controller

//user is stored in session, in order to be retrieved after redirect
@SessionAttributes("user")
public class LogInController {
    
    private Map<String, Object> resultData = new HashMap<String, Object>();
    
    /**
     * Web service needed in order to add the user type in the login form.
     * @param model
     * @return 
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView displayUser(Model model) {
        
        return new ModelAndView("index", "user", new User());
    }
    
    /**
     * Web service needed in order to add the user type in the login form.
     * @param model
     * @return 
     */
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ModelAndView displayUserProfile(Model model) {
        
        return new ModelAndView("profile", "resultData", this.resultData);
    }
    
    @RequestMapping(value = "/signIn", method = RequestMethod.POST)
    public ModelAndView signIn(@ModelAttribute("user") User user,
                               BindingResult result, ModelMap model) {
        
        Result signInResult = UserService.logIn(user.getEmail(), user.getPassword());
        
        boolean resultCode = signInResult.getResultCode();
        String redirectPage;
        String message = signInResult.getResultMessage();
        if(resultCode == true) {
            
            //successful sign up - return corresponding message
            message += " Redirecting to profile page...";
//            signInResult.setResultMessage(signInResult.getResultMessage() + " Redirecting to profile page...");
            redirectPage = "signIn";
        }
        else {
            
            redirectPage = "tryAgain";
        }
        
        //ModelAndView: Spring class that resolves the view (jsp file that will display the result)
        //and the data model (the result from the service processing, which is returned back to the
        //browser through the view)
        
        //the view is resolved in /WEB-INF/jsp/index.jsp (see dispatcher-servlet.xml for details)
        //1st argument: view that needs to be resolved
        //2nd argument: data message (the name which will be used in order to access the data model in the jsp file)
        //3rd argument: data model (usually a Java bean)
        
        //creating a map in order to send both message and user in jsp
//        System.out.println(message);
        this.resultData.put("message", signInResult.getResultMessage());
        this.resultData.put("user", signInResult.getUser());
        
        return new ModelAndView(redirectPage, "resultData", this.resultData);
        
//        return new ModelAndView(redirectPage, "message", message);
        
    }
}
