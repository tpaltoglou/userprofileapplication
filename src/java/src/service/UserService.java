/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.service;

import helper.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.derby.impl.sql.compile.RowCountNode;
import src.helper.DBConnection;
import src.model.User;

/**
 *
 * @author asus
 */
public class UserService {
    
    private static int numOfUsers = retrieveNumOfUsers();
    
    public static Result logIn(String email, String password) {
        
        Result result;
        Connection conn = null;
        PreparedStatement stmt = null;
        int rowcount = 0;
        try{

           conn = DBConnection.createConnection();
           
           //search user by her email and password
           String sql = "SELECT * FROM users WHERE email = ? AND password = ?";
           stmt = conn.prepareStatement(sql);
           stmt.setString(1, email);
           stmt.setString(2, password);
           ResultSet rs = stmt.executeQuery();
           if(rs.next()) {
               
               int userID = rs.getInt("userID");
               String name = rs.getString("name");
               email = rs.getString("email");
               String telephonenumber = rs.getString("telephonenumber");
               String companyName = rs.getString("companyName");
               
               String message = "Hello, " + name;
               System.out.println(message);
               result = new Result(message, true);
               
               User u = new User(name, email, telephonenumber, companyName, password);
               result.setUser(u);
               return result;
           }
           
         }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        result = new Result("Login failed: wrong email or password", false);
        return result;
    }
    
    /**
     * Inserts a user into the database.
     * @param u 
     */
    public static Result register(User u) {
        
        numOfUsers++;
        u.setUserID(numOfUsers);
        
        Result result;
        Connection conn = null;
        PreparedStatement stmt = null;
        String message = "";
        try{

           conn = DBConnection.createConnection();
           
           //if a user with the same email exists, do not insert user
           if(retrieveUserInDB(u) != 0) {
               
               message = "User with email " + u.getEmail() + " already exists.";
               System.out.println(message);
               result = new Result(message, false);
               return result;
           }
           
           int userID = u.getUserID();
           String name = u.getName();
           String email = u.getEmail();
           String telephoneNumber = u.getTelephoneNumber();
           String companyName = u.getCompanyName();
           String password = u.getPassword();
           String sql = "INSERT INTO users(userid, name, email, telephonenumber, companyname, password)"
                   + "VALUES(?,?,?,?,?,?)";
           stmt = conn.prepareStatement(sql);
           stmt.setInt(1, userID);
           stmt.setString(2, name);
           stmt.setString(3, email);
           stmt.setString(4, telephoneNumber);
           stmt.setString(5, companyName);
           stmt.setString(6, password);
           stmt.executeUpdate();
         }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        message = "User with email " + u.getEmail() + " is registered.";
        System.out.println(message);
        result = new Result(message, true);
        return result;
    }
    
    /**
     * Retrieves user in db by her email (needed for registration purposes).
     * @param u
     * @return 
     */
    public static int retrieveUserInDB(User u) {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        int rowcount = 0;
        try{

           conn = DBConnection.createConnection();
           
           String email = u.getEmail();
           String sql = "SELECT COUNT(*) FROM users WHERE email = ?";
           stmt = conn.prepareStatement(sql);
           stmt.setString(1, email);
           ResultSet rs = stmt.executeQuery();
           if(rs.next()) {
               
               rowcount = rs.getInt(1);
           }
           
         }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        return rowcount;
    }
    
    /**
     * Retrieves the number of the stored users. Needed to initialize the id number of each user that is
     * going to be registered.
     */
    public static int retrieveNumOfUsers() {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        int rowcount = 0;
        try{

           conn = DBConnection.createConnection();
           
           String sql = "SELECT COUNT(*) FROM users";
           stmt = conn.prepareStatement(sql);
           ResultSet rs = stmt.executeQuery();
           if(rs.next()) {
               
               rowcount = rs.getInt(1);
           }
           
         }
        catch(Exception e) {
            e.printStackTrace();
        }
        
        return rowcount;
    }
}
